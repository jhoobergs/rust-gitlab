// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crate::api::endpoint_prelude::*;
use derive_builder::Builder;
use std::collections::BTreeMap;

//TODO: fix types, lot's of strings should be enums

/// Query Settings of an instance.
#[derive(Debug, Builder)]
#[builder(setter(strip_option))]
pub struct Settings<'a> {
    #[builder(setter(into), default)]
    /// Abuse reports will be sent to this address if it is set. Abuse reports are always available in the Admin Area.
    admin_notification_email: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Where to redirect users after logout.
    after_sign_out_path: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Text shown to the user after signing up
    after_sign_up_text: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// API key for Akismet spam protection.
    akismet_api_key: Option<Cow<'a, str>>,

    #[builder(default)]
    /// (If enabled, requires: akismet_api_key) Enable or disable Akismet spam protection.
    akismet_enabled: Option<bool>,

    #[builder(default)]
    /// Set to true to allow group owners to manage LDAP
    allow_group_owners_to_manage_ldap: Option<bool>,

    #[builder(default)]
    #[deprecated(note = "Use allow_local_requests_from_web_hooks_and_services instead")]
    /// Allow requests to the local network from hooks and services.
    allow_local_requests_from_hooks_and_services: Option<bool>,

    #[builder(default)]
    /// Allow requests to the local network from system hooks.
    allow_local_requests_from_system_hooks: Option<bool>,

    #[builder(default)]
    /// Allow requests to the local network from web hooks and services.
    allow_local_requests_from_web_hooks_and_services: Option<bool>,

    #[builder(setter(into), default)]
    /// Set the duration for which the jobs will be considered as old and expired. Once that time passes, the jobs will be archived and no longer able to be retried. Make it empty to never expire jobs. It has to be no less than 1 day, for example: 15 days, 1 month, 2 years.
    archive_builds_in_human_readable: Option<Cow<'a, str>>,

    #[builder(default)]
    /// (If enabled, requires: asset_proxy_url) Enable proxying of assets. GitLab restart is required to apply changes.
    asset_proxy_enabled: Option<bool>,

    #[builder(setter(into), default)]
    /// Shared secret with the asset proxy server. GitLab restart is required to apply changes.
    asset_proxy_secret_key: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// URL of the asset proxy server. GitLab restart is required to apply changes.
    asset_proxy_url: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Assets that match these domain(s) will NOT be proxied. Wildcards allowed. Your GitLab installation URL is automatically whitelisted. GitLab restart is required to apply changes.
    asset_proxy_whitelist: Option<Cow<'a, str>>,

    #[builder(default)]
    /// By default, we write to the authorized_keys file to support Git over SSH without additional configuration. GitLab can be optimized to authenticate SSH keys via the database file. Only disable this if you have configured your OpenSSH server to use the AuthorizedKeysCommand.
    authorized_keys_enabled: Option<bool>,

    #[builder(setter(into), default)]
    /// Specify a domain to use by default for every project’s Auto Review Apps and Auto Deploy stages.
    auto_devops_domain: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Enable Auto DevOps for projects by default. It will automatically build, test, and deploy applications based on a predefined CI/CD configuration.
    auto_devops_enabled: Option<bool>,

    #[builder(default)]
    /// Enabling this will make only licensed EE features available to projects if the project namespace’s plan includes the feature or if the project is public.
    check_namespace_plan: Option<bool>,

    #[builder(setter(into), default)]
    /// Custom hostname (for private commit emails).
    commit_email_hostname: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Container Registry token duration in minutes.
    container_registry_token_expire_delay: Option<u64>,

    #[builder(setter(into), default)]
    /// Set the default expiration time for each job’s artifacts.
    default_artifacts_expire_in: Option<Cow<'a, str>>,

    #[builder(default)]
    /// The default name for a branch
    default_branch_name: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Determine if developers can push to master. Can take: 0 (not protected, both developers and maintainers can push new commits, force push, or delete the branch), 1 (partially protected, developers and maintainers can push new commits, but cannot force push, or delete, the branch) or 2 (fully protected, developers cannot push new commits, but maintainers can; no-one can force push or delete the branch) as a parameter. Default is 2.
    default_branch_protection: Option<u64>,

    #[builder(setter(into), default)]
    /// Default CI configuration path for new projects (.gitlab-ci.yml if not set).
    default_ci_config_path: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// What visibility level new groups receive. Can take private, internal and public as a parameter. Default is private.
    default_group_visibility: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Default project creation protection. Can take: 0 (No one), 1 (Maintainers) or 2 (Developers + Maintainers)
    default_project_creation: Option<u64>,

    #[builder(setter(into), default)]
    /// What visibility level new projects receive. Can take private, internal and public as a parameter. Default is private.
    default_project_visibility: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Project limit per user. Default is 100000.
    default_projects_limit: Option<u64>,

    #[builder(setter(into), default)]
    /// What visibility level new snippets receive. Can take private, internal and public as a parameter. Default is private.
    default_snippet_visibility: Option<Cow<'a, str>>,

    #[builder(default)]
    /// The number of days to wait before deleting a project or group that is marked for deletion. Value must be between 0 and 90.
    deletion_adjourned_period: Option<u64>,

    #[builder(default)]
    /// Maximum diff patch size (Bytes).
    diff_max_patch_bytes: Option<u64>,

    #[builder(setter(into), default)]
    /// Disabled OAuth sign-in sources.
    disabled_oauth_sign_in_sources: Option<Vec<Cow<'a, str>>>,

    #[builder(default)]
    /// Enforce DNS rebinding attack protection.
    dns_rebinding_protection_enabled: Option<bool>,

    #[builder(default)]
    /// (If enabled, requires: domain_blacklist) Allows blocking sign-ups from emails from specific domains.
    domain_blacklist_enabled: Option<bool>,

    #[builder(setter(into), default)]
    /// Users with e-mail addresses that match these domain(s) will NOT be able to sign-up. Wildcards allowed. Use separate lines for multiple entries. Ex: domain.com, *.domain.com.
    domain_blacklist: Option<Vec<Cow<'a, str>>>,

    #[builder(setter(into), default)]
    /// Force people to use only corporate emails for sign-up. Default is null, meaning there is no restriction.
    domain_whitelist: Option<Vec<Cow<'a, str>>>,

    #[builder(default)]
    /// The minimum allowed bit length of an uploaded DSA key. Default is 0 (no restriction). -1 disables DSA keys.
    dsa_key_restriction: Option<u64>,

    #[builder(default)]
    /// The minimum allowed curve size (in bits) of an uploaded ECDSA key. Default is 0 (no restriction). -1 disables ECDSA keys.
    ecdsa_key_restriction: Option<u64>,

    #[builder(default)]
    /// The minimum allowed curve size (in bits) of an uploaded ED25519 key. Default is 0 (no restriction). -1 disables ED25519 keys.
    ed25519_key_restriction: Option<u64>,

    #[builder(setter(into), default)]
    /// AWS IAM access key ID
    eks_access_key_id: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Amazon account ID
    eks_account_id: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Enable integration with Amazon EKS
    eks_integration_enabled: Option<bool>,

    #[builder(setter(into), default)]
    /// AWS IAM secret access key
    eks_secret_access_key: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// AWS IAM access key
    elasticsearch_aws_access_key: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// The AWS region the Elasticsearch domain is configured
    elasticsearch_aws_region: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// AWS IAM secret access key
    elasticsearch_aws_secret_access_key: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Enable the use of AWS hosted Elasticsearch
    elasticsearch_aws: Option<bool>,

    #[builder(default)]
    /// Maximum size of text fields that will be indexed by Elasticsearch. 0 value means no limit. This does not apply to repository and wiki indexing.
    elasticsearch_indexed_field_length_limit: Option<u64>,

    #[builder(default)]
    /// Maximum size of repository and wiki files that will be indexed by Elasticsearch.
    elasticsearch_indexed_file_size_limit_kb: Option<u64>,

    #[builder(default)]
    /// Enable Elasticsearch indexing
    elasticsearch_indexing: Option<bool>,

    #[builder(default)]
    /// Limit Elasticsearch to index certain namespaces and projects
    elasticsearch_limit_indexing: Option<bool>,

    #[builder(default)]
    /// Maximum concurrency of Elasticsearch bulk requests per indexing operation. This only applies to repository indexing operations.
    elasticsearch_max_bulk_concurrency: Option<u64>,

    #[builder(default)]
    /// Maximum size of Elasticsearch bulk indexing requests in MB. This only applies to repository indexing operations.
    elasticsearch_max_bulk_size_mb: Option<u64>,

    #[builder(default)]
    /// The namespaces to index via Elasticsearch if elasticsearch_limit_indexing is enabled.
    elasticsearch_namespace_ids: Option<Vec<u64>>,

    #[builder(default)]
    /// The projects to index via Elasticsearch if elasticsearch_limit_indexing is enabled.
    elasticsearch_project_ids: Option<Vec<u64>>,

    #[builder(default)]
    /// Enable Elasticsearch search
    elasticsearch_search: Option<bool>,

    #[builder(setter(into), default)]
    /// The URL to use for connecting to Elasticsearch. Use a comma-separated list to support cluster (for example, http://localhost:9200, http://localhost:9201"). If your Elasticsearch instance is password protected, pass the username:password in the URL (for example, http://<username>:<password>@<elastic_host>:9200/).
    elasticsearch_url: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Additional text added to the bottom of every email for legal/auditing/compliance reasons
    email_additional_text: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Some email servers do not support overriding the email sender name. Enable this option to include the name of the author of the issue, merge request or comment in the email body instead.
    email_author_in_body: Option<bool>,

    #[builder(setter(into), default)]
    /// Enabled protocols for Git access. Allowed values are: ssh, http, and nil to allow both protocols.
    enabled_git_access_protocol: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Enabling this permits enforcement of namespace storage limits.
    enforce_namespace_storage_limit: Option<bool>,

    #[builder(default)]
    /// (If enabled, requires: terms) Enforce application ToS to all users.
    enforce_terms: Option<bool>,

    #[builder(setter(into), default)]
    /// (If enabled, requires: external_auth_client_key) The certificate to use to authenticate with the external authorization service
    external_auth_client_cert: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Passphrase to use for the private key when authenticating with the external service this is encrypted when stored
    external_auth_client_key_pass: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Private key for the certificate when authentication is required for the external authorization service, this is encrypted when stored
    external_auth_client_key: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// The default classification label to use when requesting authorization and no classification label has been specified on the project
    external_authorization_service_default_label: Option<Cow<'a, str>>,

    #[builder(default)]
    /// (If enabled, requires: external_authorization_service_default_label, external_authorization_service_timeout and external_authorization_service_url) Enable using an external authorization service for accessing projects
    external_authorization_service_enabled: Option<bool>,

    #[builder(default)]
    /// The timeout after which an authorization request is aborted, in seconds. When a request times out, access is denied to the user. (min: 0.001, max: 10, step: 0.001)
    external_authorization_service_timeout: Option<f64>,

    #[builder(setter(into), default)]
    /// URL to which authorization requests will be directed
    external_authorization_service_url: Option<Cow<'a, str>>,

    #[builder(default)]
    /// The ID of a project to load custom file templates from
    file_template_project_id: Option<u64>,

    #[builder(default)]
    /// Start day of the week for calendar views and date pickers. Valid values are 0 (default) for Sunday, 1 for Monday, and 6 for Saturday.
    first_day_of_week: Option<u64>,

    #[builder(setter(into))]
    /// Comma-separated list of IPs and CIDRs of allowed secondary nodes. For example, 1.1.1.1, 2.2.2.0/24.
    // TODO? geo_node_allowed_ips: Cow<'a, str>,

    #[builder(default)]
    /// The amount of seconds after which a request to get a secondary node status will time out.
    geo_status_timeout: Option<u64>,

    #[builder(default)]
    /// Default Gitaly timeout, in seconds. This timeout is not enforced for Git fetch/push operations or Sidekiq jobs. Set to 0 to disable timeouts.
    gitaly_timeout_default: Option<u64>,

    #[builder(default)]
    /// Gitaly fast operation timeout, in seconds. Some Gitaly operations are expected to be fast. If they exceed this threshold, there may be a problem with a storage shard and ‘failing fast’ can help maintain the stability of the GitLab instance. Set to 0 to disable timeouts.
    gitaly_timeout_fast: Option<u64>,

    #[builder(default)]
    /// Medium Gitaly timeout, in seconds. This should be a value between the Fast and the Default timeout. Set to 0 to disable timeouts.
    gitaly_timeout_medium: Option<u64>,

    #[builder(default)]
    /// Enable Grafana.
    grafana_enabled: Option<bool>,

    #[builder(setter(into), default)]
    /// Grafana URL.
    grafana_url: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Enable Gravatar.
    gravatar_enabled: Option<bool>,

    #[builder(default)]
    /// Create new projects using hashed storage paths: Enable immutable, hash-based paths and repository names to store repositories on disk. This prevents repositories from having to be moved or renamed when the Project URL changes and may improve disk I/O performance. (Always enabled since 13.0, configuration will be removed in 14.0)
    hashed_storage_enabled: Option<bool>,

    #[builder(default)]
    /// Hide marketing-related entries from help.
    help_page_hide_commercial_content: Option<bool>,

    #[builder(setter(into), default)]
    /// Alternate support URL for help page and help dropdown.
    help_page_support_url: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Custom text displayed on the help page.
    help_page_text: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// GitLab server administrator information
    help_text: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Do not display offers from third parties within GitLab.
    hide_third_party_offers: Option<bool>,

    #[builder(setter(into), default)]
    /// Redirect to this URL when not logged in.
    home_page_url: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Enable Git pack file bitmap creation.
    housekeeping_bitmaps_enabled: Option<bool>,

    #[builder(default)]
    /// (If enabled, requires: housekeeping_bitmaps_enabled, housekeeping_full_repack_period, housekeeping_gc_period, and housekeeping_incremental_repack_period) Enable or disable Git housekeeping.
    housekeeping_enabled: Option<bool>,

    #[builder(default)]
    /// Number of Git pushes after which an incremental git repack is run.
    housekeeping_full_repack_period: Option<u64>,

    #[builder(default)]
    /// Number of Git pushes after which git gc is run.
    housekeeping_gc_period: Option<u64>,

    #[builder(default)]
    /// Number of Git pushes after which an incremental git repack is run.
    housekeeping_incremental_repack_period: Option<u64>,

    #[builder(default)]
    /// Enable HTML emails.
    html_emails_enabled: Option<bool>,

    #[builder(setter(into), default)]
    /// Sources to allow project import from, possible values: github, bitbucket, bitbucket_server, gitlab, google_code, fogbugz, git, gitlab_project, gitea, manifest, and phabricator.
    import_sources: Option<Vec<Cow<'a, str>>>,

    #[builder(default)]
    /// Max number of issue creation requests per minute per user. Disabled by default.
    issues_create_limit: Option<u64>,

    #[builder(default)]
    /// Increase this value when any cached Markdown should be invalidated.
    local_markdown_version: Option<u64>,

    #[builder(setter(into), default)]
    /// Message displayed when instance is in maintenance mode
    maintenance_mode_message: Option<Cow<'a, str>>,

    #[builder(default)]
    /// When instance is in maintenance mode, non-admin users can sign in with read-only access and make read-only API requests
    maintenance_mode: Option<bool>,

    #[builder(default)]
    /// Maximum artifacts size in MB
    max_artifacts_size: Option<u64>,

    #[builder(default)]
    /// Limit attachment size in MB
    max_attachment_size: Option<u64>,

    #[builder(default)]
    /// Maximum import size in MB. 0 for unlimited. Default = 50
    max_import_size: Option<u64>,

    #[builder(default)]
    /// Maximum size of pages repositories in MB
    max_pages_size: Option<u64>,

    #[builder(default)]
    /// Maximum allowable lifetime for personal access tokens in days
    max_personal_access_token_lifetime: Option<u64>,

    #[builder(default)]
    /// A method call is only tracked when it takes longer than the given amount of milliseconds.
    metrics_method_call_threshold: Option<u64>,

    #[builder(default)]
    /// Allow repository mirroring to configured by project Maintainers. If disabled, only Admins will be able to configure repository mirroring.
    mirror_available: Option<bool>,

    #[builder(default)]
    /// Minimum capacity to be available before scheduling more mirrors preemptively
    mirror_capacity_threshold: Option<u64>,

    #[builder(default)]
    /// Maximum number of mirrors that can be synchronizing at the same time.
    mirror_max_capacity: Option<u64>,

    #[builder(default)]
    /// Maximum time (in minutes) between updates that a mirror can have when scheduled to synchronize.
    mirror_max_delay: Option<u64>,

    #[builder(default)]
    /// Use npmjs.org as a default remote repository when the package is not found in the GitLab NPM Registry
    npm_package_requests_forwarding: Option<bool>,

    #[builder(setter(into), default)]
    /// Define a list of trusted domains or ip addresses to which local requests are allowed when local requests for hooks and services are disabled.
    outbound_local_requests_whitelist: Option<Vec<Cow<'a, str>>>,

    #[builder(default)]
    /// Require users to prove ownership of custom domains. Domain verification is an essential security measure for public GitLab sites. Users are required to demonstrate they control a domain before it is enabled.
    pages_domain_verification_enabled: Option<bool>,

    #[builder(default)]
    /// Enable authentication for Git over HTTP(S) via a GitLab account password. Default is true.
    password_authentication_enabled_for_git: Option<bool>,

    #[builder(default)]
    /// Enable authentication for the web interface via a GitLab account password. Default is true.
    password_authentication_enabled_for_web: Option<bool>,

    #[builder(setter(into), default)]
    #[deprecated(note = "Use performance_bar_allowed_group_path instead")]
    /// Path of the group that is allowed to toggle the performance bar.
    performance_bar_allowed_group_id: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Path of the group that is allowed to toggle the performance bar.
    performance_bar_allowed_group_path: Option<Cow<'a, str>>,

    #[builder(default)]
    #[deprecated(note = "Pass performance_bar_allowed_group_path: nil instead")]
    /// Allow enabling the performance bar.
    performance_bar_enabled: Option<bool>,

    #[builder(default)]
    /// (If enabled, requires: plantuml_url) Enable PlantUML integration. Default is false.
    plantuml_enabled: Option<bool>,

    #[builder(setter(into), default)]
    /// The PlantUML instance URL for integration.
    plantuml_url: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Interval multiplier used by endpoints that perform polling. Set to 0 to disable polling.
    polling_interval_multiplier: Option<f64>,

    #[builder(default)]
    /// Enable project export.
    project_export_enabled: Option<bool>,

    #[builder(default)]
    /// Enable Prometheus metrics.
    prometheus_metrics_enabled: Option<bool>,

    #[builder(default)]
    /// Environment variables are protected by default.
    protected_ci_variables: Option<bool>,

    #[builder(default)]
    /// When enabled, GitLab will run a background job that will produce pseudonymized CSVs of the GitLab database that will be uploaded to your configured object storage directory.
    pseudonymizer_enabled: Option<bool>,

    #[builder(default)]
    /// Number of changes (branches or tags) in a single push to determine whether individual push events or bulk push events will be created. Bulk push events will be created if it surpasses that value.
    push_event_activities_limit: Option<u64>,

    #[builder(default)]
    /// Number of changes (branches or tags) in a single push to determine whether webhooks and services will be fired or not. Webhooks and services won’t be submitted if it surpasses that value.
    push_event_hooks_limit: Option<u64>,

    #[builder(default)]
    /// Max number of requests per minute for each raw path. Default: 300. To disable throttling set to 0.
    raw_blob_request_limit: Option<u64>,

    #[builder(default)]
    /// (If enabled, requires: recaptcha_private_key and recaptcha_site_key) Enable reCAPTCHA.
    recaptcha_enabled: Option<bool>,

    #[builder(setter(into), default)]
    /// Private key for reCAPTCHA.
    recaptcha_private_key: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Site key for reCAPTCHA.
    recaptcha_site_key: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Maximum push size (MB).
    receive_max_input_size: Option<u64>,

    #[builder(default)]
    /// GitLab will periodically run git fsck in all project and wiki repositories to look for silent disk corruption issues.
    repository_checks_enabled: Option<bool>,

    #[builder(default)]
    /// Size limit per repository (MB)
    repository_size_limit: Option<u64>,

    #[builder(setter(into), default)]
    /// (GitLab 13.1 and later) Hash of names of taken from gitlab.yml to weights. New projects are created in one of these stores, chosen by a weighted random selection.
    repository_storages_weighted: Option<BTreeMap<Cow<'a, str>, u64>>,

    #[builder(setter(into), default)]
    /// (GitLab 13.0 and earlier) List of names of enabled storage paths, taken from gitlab.yml. New projects are created in one of these stores, chosen at random.
    repository_storages: Option<Vec<Cow<'a, str>>>,

    #[builder(default)]
    /// (If enabled, requires: two_factor_grace_period) Require all users to set up Two-factor authentication.
    require_two_factor_authentication: Option<bool>,

    #[builder(setter(into), default)]
    /// Selected levels cannot be used by non-admin users for groups, projects or snippets. Can take private, internal and public as a parameter. Default is null which means there is no restriction.
    restricted_visibility_levels: Option<Vec<Cow<'a, str>>>,

    #[builder(default)]
    /// The minimum allowed bit length of an uploaded RSA key. Default is 0 (no restriction). -1 disables RSA keys.
    rsa_key_restriction: Option<u64>,

    #[builder(default)]
    /// Send confirmation email on sign-up.
    send_user_confirmation_email: Option<bool>,

    #[builder(default)]
    /// Session duration in minutes. GitLab restart is required to apply changes
    session_expire_delay: Option<u64>,

    #[builder(default)]
    /// (If enabled, requires: shared_runners_text and shared_runners_minutes) Enable shared runners for new projects.
    shared_runners_enabled: Option<bool>,

    #[builder(default)]
    /// Set the maximum number of pipeline minutes that a group can use on shared runners per month.
    shared_runners_minutes: Option<u64>,

    #[builder(setter(into), default)]
    /// Shared runners text.
    shared_runners_text: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Text on the login page.
    sign_in_text: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    #[deprecated(note = "Use password_authentication_enabled_for_web instead")]
    /// Flag indicating if password authentication is enabled for the web interface.
    signin_enabled: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Enable registration. Default is true.
    signup_enabled: Option<bool>,

    #[builder(default)]
    /// (If enabled, requires: slack_app_id, slack_app_secret and slack_app_secret) Enable Slack app.
    slack_app_enabled: Option<bool>,

    #[builder(setter(into), default)]
    /// The app ID of the Slack-app.
    slack_app_id: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// The app secret of the Slack-app.
    slack_app_secret: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// The verification token of the Slack-app.
    slack_app_verification_token: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Max snippet content size in bytes. Default: 52428800 Bytes (50MB).
    snippet_size_limit: Option<u64>,

    #[builder(setter(into), default)]
    /// The Snowplow site name / application ID. (for example, gitlab)
    snowplow_app_id: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// The Snowplow collector hostname. (for example, snowplow.trx.gitlab.net)
    snowplow_collector_hostname: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// The Snowplow cookie domain. (for example, .gitlab.com)
    snowplow_cookie_domain: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Enable snowplow tracking.
    snowplow_enabled: Option<bool>,

    #[builder(setter(into), default)]
    /// The Snowplow base Iglu Schema Registry URL to use for custom context and self describing events’
    snowplow_iglu_registry_url: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Enables Sourcegraph integration. Default is false. If enabled, requires sourcegraph_url.
    sourcegraph_enabled: Option<bool>,

    #[builder(default)]
    /// Blocks Sourcegraph from being loaded on private and internal projects. Default is true.
    sourcegraph_public_only: Option<bool>,

    #[builder(setter(into), default)]
    /// The Sourcegraph instance URL for integration.
    sourcegraph_url: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Enables Spam Check via external API endpoint. Default is false.
    spam_check_endpoint_enabled: Option<bool>,

    #[builder(setter(into), default)]
    /// URL of the external Spam Check service endpoint.
    spam_check_endpoint_url: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Maximum time for web terminal websocket connection (in seconds). Set to 0 for unlimited time.
    terminal_max_session_time: Option<u64>,

    #[builder(setter(into), default)]
    /// (Required by: enforce_terms) Markdown content for the ToS.
    terms: Option<Cow<'a, str>>,

    #[builder(default)]
    /// (If enabled, requires: throttle_authenticated_api_period_in_seconds and throttle_authenticated_api_requests_per_period) Enable authenticated API request rate limit. Helps reduce request volume (for example, from crawlers or abusive bots).
    throttle_authenticated_api_enabled: Option<bool>,

    #[builder(default)]
    /// Rate limit period in seconds.
    throttle_authenticated_api_period_in_seconds: Option<u64>,

    #[builder(default)]
    /// Max requests per period per user.
    throttle_authenticated_api_requests_per_period: Option<u64>,

    #[builder(default)]
    /// (If enabled, requires: throttle_authenticated_web_period_in_seconds and throttle_authenticated_web_requests_per_period) Enable authenticated web request rate limit. Helps reduce request volume (for example, from crawlers or abusive bots).
    throttle_authenticated_web_enabled: Option<bool>,

    #[builder(default)]
    /// Rate limit period in seconds.
    throttle_authenticated_web_period_in_seconds: Option<u64>,

    #[builder(default)]
    /// Max requests per period per user.
    throttle_authenticated_web_requests_per_period: Option<u64>,

    #[builder(default)]
    /// (If enabled, requires: throttle_unauthenticated_period_in_seconds and throttle_unauthenticated_requests_per_period) Enable unauthenticated request rate limit. Helps reduce request volume (for example, from crawlers or abusive bots).
    throttle_unauthenticated_enabled: Option<bool>,

    #[builder(default)]
    /// Rate limit period in seconds.
    throttle_unauthenticated_period_in_seconds: Option<u64>,

    #[builder(default)]
    /// Max requests per period per IP.
    throttle_unauthenticated_requests_per_period: Option<u64>,

    #[builder(default)]
    /// Limit display of time tracking units to hours. Default is false.
    time_tracking_limit_to_hours: Option<bool>,

    #[builder(default)]
    /// Amount of time (in hours) that users are allowed to skip forced configuration of two-factor authentication.
    two_factor_grace_period: Option<u64>,

    #[builder(default)]
    /// (If enabled, requires: unique_ips_limit_per_user and unique_ips_limit_time_window) Limit sign in from multiple ips.
    unique_ips_limit_enabled: Option<bool>,

    #[builder(default)]
    /// Maximum number of IPs per user.
    unique_ips_limit_per_user: Option<u64>,

    #[builder(default)]
    /// How many seconds an IP will be counted towards the limit.
    unique_ips_limit_time_window: Option<u64>,

    #[builder(default)]
    /// Every week GitLab will report license usage back to GitLab, Inc.
    usage_ping_enabled: Option<bool>,

    #[builder(default)]
    /// Newly registered users will be external by default.
    user_default_external: Option<bool>,

    #[builder(setter(into), default)]
    /// Specify an e-mail address regex pattern to identify default internal users.
    user_default_internal_regex: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Allow users to register any application to use GitLab as an OAuth provider.
    user_oauth_applications: Option<bool>,

    #[builder(default)]
    /// When set to false disable the “You won’t be able to pull or push project code via SSH” warning shown to users with no uploaded SSH key.
    user_show_add_ssh_key_message: Option<bool>,

    #[builder(default)]
    /// Let GitLab inform you when an update is available.
    version_check_enabled: Option<bool>,

    #[builder(default)]
    /// Live Preview (allow live previews of JavaScript projects in the Web IDE using CodeSandbox Live Preview).
    web_ide_clientside_preview_enabled: Option<bool>,

    #[builder(default)]
    /// Maximum wiki page content size in bytes. Default: 52428800 Bytes (50 MB). The minimum value is 1024 bytes.
    wiki_page_max_content_bytes: Option<u64>,
}

impl<'a> Settings<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> SettingsBuilder<'a> {
        SettingsBuilder::default()
    }
}

impl<'a> Endpoint for Settings<'a> {
    fn method(&self) -> Method {
        Method::PUT
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("application/settings").into()
    }

    fn parameters(&self) -> QueryParams {
        let mut params = QueryParams::default();

        params.push_opt(
            "admin_notification_email",
            self.admin_notification_email.as_ref(),
        );
        params.push_opt("after_sign_out_path", self.after_sign_out_path.as_ref());
        params.push_opt("after_sign_up_text", self.after_sign_up_text.as_ref());
        params.push_opt("akismet_api_key", self.akismet_api_key.as_ref());
        params.push_opt("akismet_enabled", self.akismet_enabled);
        params.push_opt(
            "allow_group_owners_to_manage_ldap",
            self.allow_group_owners_to_manage_ldap,
        );
        #[allow(deprecated)]
        {
            params.push_opt(
                "allow_local_requests_from_hooks_and_services",
                self.allow_local_requests_from_hooks_and_services,
            );
        }
        params.push_opt(
            "allow_local_requests_from_system_hooks",
            self.allow_local_requests_from_system_hooks,
        );
        params.push_opt(
            "allow_local_requests_from_web_hooks_and_services",
            self.allow_local_requests_from_web_hooks_and_services,
        );
        params.push_opt(
            "archive_builds_in_human_readable",
            self.archive_builds_in_human_readable.as_ref(),
        );
        params.push_opt("asset_proxy_enabled", self.asset_proxy_enabled);
        params.push_opt(
            "asset_proxy_secret_key",
            self.asset_proxy_secret_key.as_ref(),
        );
        params.push_opt("asset_proxy_url", self.asset_proxy_url.as_ref());
        params.push_opt("asset_proxy_whitelist", self.asset_proxy_whitelist.as_ref());
        params.push_opt("authorized_keys_enabled", self.authorized_keys_enabled);
        params.push_opt("auto_devops_domain", self.auto_devops_domain.as_ref());
        params.push_opt("auto_devops_enabled", self.auto_devops_enabled);
        params.push_opt("check_namespace_plan", self.check_namespace_plan);
        params.push_opt("commit_email_hostname", self.commit_email_hostname.as_ref());
        params.push_opt(
            "container_registry_token_expire_delay",
            self.container_registry_token_expire_delay,
        );
        params.push_opt(
            "default_artifacts_expire_in",
            self.default_artifacts_expire_in.as_ref(),
        );
        params.push_opt("default_branch_name", self.default_branch_name.as_ref());
        params.push_opt("default_branch_protection", self.default_branch_protection);
        params.push_opt(
            "default_ci_config_path",
            self.default_ci_config_path.as_ref(),
        );
        params.push_opt(
            "default_group_visibility",
            self.default_group_visibility.as_ref(),
        );
        params.push_opt("default_project_creation", self.default_project_creation);
        params.push_opt(
            "default_project_visibility",
            self.default_project_visibility.as_ref(),
        );
        params.push_opt("default_projects_limit", self.default_projects_limit);
        params.push_opt(
            "default_snippet_visibility",
            self.default_snippet_visibility.as_ref(),
        );
        params.push_opt("deletion_adjourned_period", self.deletion_adjourned_period);
        params.push_opt("diff_max_patch_bytes", self.diff_max_patch_bytes);
        if let Some(disabled_oauth_sign_in_sources) = self.disabled_oauth_sign_in_sources.as_ref() {
            params.extend(
                disabled_oauth_sign_in_sources
                    .iter()
                    .map(|value| (format!("disabled_oauth_sign_in_sources[]"), value)),
            );
        }
        params.push_opt(
            "dns_rebinding_protection_enabled",
            self.dns_rebinding_protection_enabled,
        );
        params.push_opt("domain_blacklist_enabled", self.domain_blacklist_enabled);
        if let Some(domain_blacklist) = self.domain_blacklist.as_ref() {
            params.extend(
                domain_blacklist
                    .iter()
                    .map(|value| (format!("domain_blacklist[]"), value)),
            );
        }
        if let Some(domain_whitelist) = self.domain_whitelist.as_ref() {
            params.extend(
                domain_whitelist
                    .iter()
                    .map(|value| (format!("domain_whitelist[]"), value)),
            );
        }
        params.push_opt("dsa_key_restriction", self.dsa_key_restriction);
        params.push_opt("ecdsa_key_restriction", self.ecdsa_key_restriction);
        params.push_opt("ed25519_key_restriction", self.ed25519_key_restriction);
        params.push_opt("eks_access_key_id", self.eks_access_key_id.as_ref());
        params.push_opt("eks_account_id", self.eks_account_id.as_ref());
        params.push_opt("eks_integration_enabled", self.eks_integration_enabled);
        params.push_opt("eks_secret_access_key", self.eks_secret_access_key.as_ref());
        params.push_opt(
            "elasticsearch_aws_access_key",
            self.elasticsearch_aws_access_key.as_ref(),
        );
        params.push_opt(
            "elasticsearch_aws_region",
            self.elasticsearch_aws_region.as_ref(),
        );
        params.push_opt(
            "elasticsearch_aws_secret_access_key",
            self.elasticsearch_aws_secret_access_key.as_ref(),
        );
        params.push_opt("elasticsearch_aws", self.elasticsearch_aws);
        params.push_opt(
            "elasticsearch_indexed_field_length_limit",
            self.elasticsearch_indexed_field_length_limit,
        );
        params.push_opt(
            "elasticsearch_indexed_file_size_limit_kb",
            self.elasticsearch_indexed_file_size_limit_kb,
        );
        params.push_opt("elasticsearch_indexing", self.elasticsearch_indexing);
        params.push_opt(
            "elasticsearch_limit_indexing",
            self.elasticsearch_limit_indexing,
        );
        params.push_opt(
            "elasticsearch_max_bulk_concurrency",
            self.elasticsearch_max_bulk_concurrency,
        );
        params.push_opt(
            "elasticsearch_max_bulk_size_mb",
            self.elasticsearch_max_bulk_size_mb,
        );
        if let Some(elasticsearch_namespace_ids) = self.elasticsearch_namespace_ids.as_ref() {
            params.extend(
                elasticsearch_namespace_ids
                    .clone()
                    .into_iter()
                    .map(|value| (format!("elasticsearch_namespace_ids[]"), value)),
            );
        }
        if let Some(elasticsearch_project_ids) = self.elasticsearch_project_ids.as_ref() {
            params.extend(
                elasticsearch_project_ids
                    .clone()
                    .into_iter()
                    .map(|value| (format!("elasticsearch_projects_ids[]"), value)),
            );
        }
        params.push_opt("elasticsearch_search", self.elasticsearch_search);
        params.push_opt("elasticsearch_url", self.elasticsearch_url.as_ref());
        params.push_opt("email_additional_text", self.email_additional_text.as_ref());
        params.push_opt("email_author_in_body", self.email_author_in_body);
        params.push_opt(
            "enabled_git_access_protocol",
            self.enabled_git_access_protocol.as_ref(),
        );
        params.push_opt(
            "enforce_namespace_storage_limit",
            self.enforce_namespace_storage_limit,
        );
        params.push_opt("enforce_terms", self.enforce_terms);
        params.push_opt(
            "external_auth_client_cert",
            self.external_auth_client_cert.as_ref(),
        );
        params.push_opt(
            "external_auth_client_key_pass",
            self.external_auth_client_key_pass.as_ref(),
        );
        params.push_opt(
            "external_auth_client_key",
            self.external_auth_client_key.as_ref(),
        );
        params.push_opt(
            "external_authorization_service_default_label",
            self.external_authorization_service_default_label.as_ref(),
        );
        params.push_opt(
            "external_authorization_service_enabled",
            self.external_authorization_service_enabled,
        );
        params.push_opt(
            "external_authorization_service_timeout",
            self.external_authorization_service_timeout,
        );
        params.push_opt(
            "external_authorization_service_url",
            self.external_authorization_service_url.as_ref(),
        );
        params.push_opt("file_template_project_id", self.file_template_project_id);
        params.push_opt("first_day_of_week", self.first_day_of_week);
        //params.push("geo_node_allowed_ips", self.geo_node_allowed_ips); //TODO?
        params.push_opt("geo_status_timeout", self.geo_status_timeout);
        params.push_opt("gitaly_timeout_default", self.gitaly_timeout_default);
        params.push_opt("gitaly_timeout_fast", self.gitaly_timeout_fast);
        params.push_opt("gitaly_timeout_medium", self.gitaly_timeout_medium);
        params.push_opt("grafana_enabled", self.grafana_enabled);
        params.push_opt("grafana_url", self.grafana_url.as_ref());
        params.push_opt("gravatar_enabled", self.gravatar_enabled);
        params.push_opt("hashed_storage_enabled", self.hashed_storage_enabled);
        params.push_opt(
            "help_page_hide_commercial_content",
            self.help_page_hide_commercial_content,
        );
        params.push_opt("help_page_support_url", self.help_page_support_url.as_ref());
        params.push_opt("help_page_text", self.help_page_text.as_ref());
        params.push_opt("help_text", self.help_text.as_ref());
        params.push_opt("hide_third_party_offers", self.hide_third_party_offers);
        params.push_opt("home_page_url", self.home_page_url.as_ref());
        params.push_opt(
            "housekeeping_bitmaps_enabled",
            self.housekeeping_bitmaps_enabled,
        );
        params.push_opt("housekeeping_enabled", self.housekeeping_enabled);
        params.push_opt(
            "housekeeping_full_repack_period",
            self.housekeeping_full_repack_period,
        );
        params.push_opt("housekeeping_gc_period", self.housekeeping_gc_period);
        params.push_opt(
            "housekeeping_incremental_repack_period",
            self.housekeeping_incremental_repack_period,
        );
        params.push_opt("html_emails_enabled", self.html_emails_enabled);
        if let Some(import_sources) = self.import_sources.as_ref() {
            //TODO: type
            params.extend(
                import_sources
                    .iter()
                    .map(|value| (format!("import_sources[]"), value)),
            );
        }
        params.push_opt("issues_create_limit", self.issues_create_limit);
        params.push_opt("local_markdown_version", self.local_markdown_version);
        params.push_opt(
            "maintenance_mode_message",
            self.maintenance_mode_message.as_ref(),
        );
        params.push_opt("maintenance_mode", self.maintenance_mode);
        params.push_opt("max_artifacts_size", self.max_artifacts_size);
        params.push_opt("max_attachment_size", self.max_attachment_size);
        params.push_opt("max_import_size", self.max_import_size);
        params.push_opt("max_pages_size", self.max_pages_size);
        params.push_opt(
            "max_personal_access_token_lifetime",
            self.max_personal_access_token_lifetime,
        );
        params.push_opt(
            "metrics_method_call_threshold",
            self.metrics_method_call_threshold,
        );
        params.push_opt("mirror_available", self.mirror_available);
        params.push_opt("mirror_capacity_threshold", self.mirror_capacity_threshold);
        params.push_opt("mirror_max_capacity", self.mirror_max_capacity);
        params.push_opt("mirror_max_delay", self.mirror_max_delay);
        params.push_opt(
            "npm_package_requests_forwarding",
            self.npm_package_requests_forwarding,
        );
        if let Some(outbound_local_requests_whitelist) =
            self.outbound_local_requests_whitelist.as_ref()
        {
            params.extend(
                outbound_local_requests_whitelist
                    .iter()
                    .map(|value| (format!("outbound_local_requests_whitelist[]"), value)),
            );
        }
        params.push_opt(
            "pages_domain_verification_enabled",
            self.pages_domain_verification_enabled,
        );
        params.push_opt(
            "password_authentication_enabled_for_git",
            self.password_authentication_enabled_for_git,
        );
        params.push_opt(
            "password_authentication_enabled_for_web",
            self.password_authentication_enabled_for_web,
        );
        #[allow(deprecated)]
        {
            params.push_opt(
                "performance_bar_allowed_group_id",
                self.performance_bar_allowed_group_id.as_ref(),
            );
        }
        params.push_opt(
            "performance_bar_allowed_group_path",
            self.performance_bar_allowed_group_path.as_ref(),
        );
        #[allow(deprecated)]
        {
            params.push_opt("performance_bar_enabled", self.performance_bar_enabled);
        }
        params.push_opt("plantuml_enabled", self.plantuml_enabled);
        params.push_opt("plantuml_url", self.plantuml_url.as_ref());
        params.push_opt(
            "polling_interval_multiplier",
            self.polling_interval_multiplier,
        );
        params.push_opt("project_export_enabled", self.project_export_enabled);
        params.push_opt(
            "prometheus_metrics_enabled",
            self.prometheus_metrics_enabled,
        );
        params.push_opt("protected_ci_variables", self.protected_ci_variables);
        params.push_opt("pseudonymizer_enabled", self.pseudonymizer_enabled);
        params.push_opt(
            "push_event_activities_limit",
            self.push_event_activities_limit,
        );
        params.push_opt("push_event_hooks_limit", self.push_event_hooks_limit);
        params.push_opt("raw_blob_request_limit", self.raw_blob_request_limit);
        params.push_opt("recaptcha_enabled", self.recaptcha_enabled);
        params.push_opt("recaptcha_private_key", self.recaptcha_private_key.as_ref());
        params.push_opt("recaptcha_site_key", self.recaptcha_site_key.as_ref());
        params.push_opt("receive_max_input_size", self.receive_max_input_size);
        params.push_opt("repository_checks_enabled", self.repository_checks_enabled);
        params.push_opt("repository_size_limit", self.repository_size_limit);
        if let Some(repository_storages_weighted) = self.repository_storages_weighted.as_ref() {
            params.extend(
                repository_storages_weighted
                    .clone()
                    .into_iter()
                    .map(|(key, value)| (format!("repository_storages_weighted[{}]", key), value)),
            );
        }
        if let Some(repository_storages) = self.repository_storages.as_ref() {
            params.extend(
                repository_storages
                    .iter()
                    .map(|value| (format!("repository_storages[]"), value)),
            );
        }
        params.push_opt(
            "require_two_factor_authentication",
            self.require_two_factor_authentication,
        );
        if let Some(restricted_visibility_levels) = self.restricted_visibility_levels.as_ref() {
            params.extend(
                restricted_visibility_levels
                    .iter()
                    .map(|value| (format!("restricted_visibility_levels[]"), value)),
            );
        }
        params.push_opt("rsa_key_restriction", self.rsa_key_restriction);
        params.push_opt(
            "send_user_confirmation_email",
            self.send_user_confirmation_email,
        );
        params.push_opt("session_expire_delay", self.session_expire_delay);
        params.push_opt("shared_runners_enabled", self.shared_runners_enabled);
        params.push_opt("shared_runners_minutes", self.shared_runners_minutes);
        params.push_opt("shared_runners_text", self.shared_runners_text.as_ref());
        params.push_opt("sign_in_text", self.sign_in_text.as_ref());
        #[allow(deprecated)]
        {
            params.push_opt("signin_enabled", self.signin_enabled.as_ref());
        }
        params.push_opt("signup_enabled", self.signup_enabled);
        params.push_opt("slack_app_enabled", self.slack_app_enabled);
        params.push_opt("slack_app_id", self.slack_app_id.as_ref());
        params.push_opt("slack_app_secret", self.slack_app_secret.as_ref());
        params.push_opt(
            "slack_app_verification_token",
            self.slack_app_verification_token.as_ref(),
        );
        params.push_opt("snippet_size_limit", self.snippet_size_limit);
        params.push_opt("snowplow_app_id", self.snowplow_app_id.as_ref());
        params.push_opt(
            "snowplow_collector_hostname",
            self.snowplow_collector_hostname.as_ref(),
        );
        params.push_opt(
            "snowplow_cookie_domain",
            self.snowplow_cookie_domain.as_ref(),
        );
        params.push_opt("snowplow_enabled", self.snowplow_enabled);
        params.push_opt(
            "snowplow_iglu_registry_url",
            self.snowplow_iglu_registry_url.as_ref(),
        );
        params.push_opt("sourcegraph_enabled", self.sourcegraph_enabled);
        params.push_opt("sourcegraph_public_only", self.sourcegraph_public_only);
        params.push_opt("sourcegraph_url", self.sourcegraph_url.as_ref());
        params.push_opt(
            "spam_check_endpoint_enabled",
            self.spam_check_endpoint_enabled,
        );
        params.push_opt(
            "spam_check_endpoint_url",
            self.spam_check_endpoint_url.as_ref(),
        );
        params.push_opt("terminal_max_session_time", self.terminal_max_session_time);
        params.push_opt("terms", self.terms.as_ref());
        params.push_opt(
            "throttle_authenticated_api_enabled",
            self.throttle_authenticated_api_enabled,
        );
        params.push_opt(
            "throttle_authenticated_api_period_in_seconds",
            self.throttle_authenticated_api_period_in_seconds,
        );
        params.push_opt(
            "throttle_authenticated_api_requests_per_period",
            self.throttle_authenticated_api_requests_per_period,
        );
        params.push_opt(
            "throttle_authenticated_web_enabled",
            self.throttle_authenticated_web_enabled,
        );
        params.push_opt(
            "throttle_authenticated_web_period_in_seconds",
            self.throttle_authenticated_web_period_in_seconds,
        );
        params.push_opt(
            "throttle_authenticated_web_requests_per_period",
            self.throttle_authenticated_web_requests_per_period,
        );
        params.push_opt(
            "throttle_unauthenticated_enabled",
            self.throttle_unauthenticated_enabled,
        );
        params.push_opt(
            "throttle_unauthenticated_period_in_seconds",
            self.throttle_unauthenticated_period_in_seconds,
        );
        params.push_opt(
            "throttle_unauthenticated_requests_per_period",
            self.throttle_unauthenticated_requests_per_period,
        );
        params.push_opt(
            "time_tracking_limit_to_hours",
            self.time_tracking_limit_to_hours,
        );
        params.push_opt("two_factor_grace_period", self.two_factor_grace_period);
        params.push_opt("unique_ips_limit_enabled", self.unique_ips_limit_enabled);
        params.push_opt("unique_ips_limit_per_user", self.unique_ips_limit_per_user);
        params.push_opt(
            "unique_ips_limit_time_window",
            self.unique_ips_limit_time_window,
        );
        params.push_opt("usage_ping_enabled", self.usage_ping_enabled);
        params.push_opt("user_default_external", self.user_default_external);
        params.push_opt(
            "user_default_internal_regex",
            self.user_default_internal_regex.as_ref(),
        );
        params.push_opt("user_oauth_applications", self.user_oauth_applications);
        params.push_opt(
            "user_show_add_ssh_key_message",
            self.user_show_add_ssh_key_message,
        );
        params.push_opt("version_check_enabled", self.version_check_enabled);
        params.push_opt(
            "web_ide_clientside_preview_enabled",
            self.web_ide_clientside_preview_enabled,
        );
        params.push_opt(
            "wiki_page_max_content_bytes",
            self.wiki_page_max_content_bytes,
        );

        params
    }
}

#[cfg(test)]
mod tests {
    use crate::api::application::Settings;
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn nothing_is_sufficient() {
        Settings::builder().build().unwrap();
    }
    // TODO
}
