// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crate::api::endpoint_prelude::*;
use derive_builder::Builder;

/// Query appearance of a specific instance.
#[derive(Debug, Builder)]
#[builder(setter(strip_option))]
pub struct Appearance<'a> {
    #[builder(setter(into), default)]
    /// Instance title on the sign in / sign up page
    title: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Markdown text shown on the sign in / sign up page
    description: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Markdown text shown on the new project page
    new_project_guidelines: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Markdown text shown on the profile page below Public Avatar
    profile_image_guidelines: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Message within the system header bar
    header_message: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Message within the system footer bar
    footer_message: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Background color for the system header / footer bar
    message_background_color: Option<Cow<'a, str>>,

    #[builder(setter(into), default)]
    /// Font color for the system header / footer bar
    message_font_color: Option<Cow<'a, str>>,

    #[builder(default)]
    /// Add header and footer to all outgoing emails if enabled
    email_header_and_footer_enabled: Option<bool>,
}

impl<'a> Appearance<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> AppearanceBuilder<'a> {
        AppearanceBuilder::default()
    }
}

impl<'a> Endpoint for Appearance<'a> {
    fn method(&self) -> Method {
        Method::PUT
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("application/appearance").into()
    }

    fn parameters(&self) -> QueryParams {
        let mut params = QueryParams::default();
        params.push_opt("title", self.title.as_ref());
        params.push_opt("description", self.description.as_ref());
        params.push_opt(
            "new_project_guidelines",
            self.new_project_guidelines.as_ref(),
        );
        params.push_opt(
            "profile_image_guidelines",
            self.profile_image_guidelines.as_ref(),
        );
        params.push_opt("header_message", self.header_message.as_ref());
        params.push_opt("footer_message", self.footer_message.as_ref());
        params.push_opt(
            "message_background_color",
            self.message_background_color.as_ref(),
        );
        params.push_opt("message_font_color", self.message_font_color.as_ref());
        params.push_opt(
            "email_header_and_footer_enabled",
            self.email_header_and_footer_enabled,
        );

        /*if let Some(disabled_oauth_sign_in_sources) = self.disabled_oauth_sign_in_sources.as_ref() {
            params.extend(
                disabled_oauth_sign_in_sources
                    .iter()
                    .map(|value| (format!("disabled_oauth_sign_in_sources[]"), value)),
            );
        }*/

        params
    }
}

#[cfg(test)]
mod tests {
    use crate::api::application::Appearance;
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn nothing_is_sufficient() {
        Appearance::builder().build().unwrap();
    }
    // TODO
}
