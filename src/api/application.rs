// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![allow(clippy::module_inception)]

//! Application-related API endpoints
//!
//! These endpoints are used for querying and modifying the application

mod appearance;
mod settings;

pub use settings::Settings;
pub use settings::SettingsBuilder;

pub use appearance::Appearance;
pub use appearance::AppearanceBuilder;
