// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Set a custom attribute on a group.
#[derive(Debug, Builder)]
#[builder(setter(strip_option))]
pub struct SetGroupCustomAttribute<'a> {
    /// The group to set the custom attribute on.
    #[builder(setter(into))]
    group: NameOrId<'a>,
    /// The key of the custom attribute.
    #[builder(setter(into))]
    key: Cow<'a, str>,
    /// The value of the custom attribute.
    #[builder(setter(into))]
    value: Cow<'a, str>,
}

impl<'a> SetGroupCustomAttribute<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> SetGroupCustomAttributeBuilder<'a> {
        SetGroupCustomAttributeBuilder::default()
    }
}

impl<'a> Endpoint for SetGroupCustomAttribute<'a> {
    fn method(&self) -> Method {
        Method::PUT
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("groups/{}/custom_attributes/{}", self.group, self.key).into()
    }

    fn body(&self) -> Result<Option<(&'static str, Vec<u8>)>, BodyError> {
        let mut params = FormParams::default();

        params.push("value", &self.value);

        params.into_body()
    }
}

#[cfg(test)]
mod tests {
    use http::Method;

    use crate::api::groups::custom_attributes::SetGroupCustomAttribute;
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn group_is_necessary() {
        let err = SetGroupCustomAttribute::builder()
            .key("key")
            .value("value")
            .build()
            .unwrap_err();
        assert_eq!(err, "`group` must be initialized");
    }

    #[test]
    fn key_is_necessary() {
        let err = SetGroupCustomAttribute::builder()
            .group("group")
            .value("value")
            .build()
            .unwrap_err();
        assert_eq!(err, "`key` must be initialized");
    }

    #[test]
    fn value_is_necessary() {
        let err = SetGroupCustomAttribute::builder()
            .group("group")
            .key("key")
            .build()
            .unwrap_err();
        assert_eq!(err, "`value` must be initialized");
    }

    #[test]
    fn group_and_key_and_value_are_sufficient() {
        SetGroupCustomAttribute::builder()
            .group("group")
            .key("key")
            .value("value")
            .build()
            .unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::PUT)
            .endpoint("groups/simple%2Fgroup/custom_attributes/some_key")
            .content_type("application/x-www-form-urlencoded")
            .body_str("value=some_value")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = SetGroupCustomAttribute::builder()
            .group("simple/group")
            .key("some_key")
            .value("some_value")
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
