// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.
use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Query to list the repository tree for a project.
#[derive(Debug, Builder)]
#[builder(setter(strip_option))]
pub struct Tree<'a> {
    /// The project to list the tree for.
    #[builder(setter(into))]
    project: NameOrId<'a>,
    /// The path inside a repository. Used to get the content of subdirectories.
    #[builder(setter(into), default)]
    path: Option<Cow<'a, str>>,
    /// The name of a repository branch or tag or if not given the default branch
    #[builder(setter(into), default)]
    r#ref: Option<Cow<'a, str>>,
    /// Boolean value used to get a recursive tree (false by default)  
    #[builder(setter(into), default)]
    recursive: Option<bool>,
}

impl<'a> Tree<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> TreeBuilder<'a> {
        TreeBuilder::default()
    }
}

impl<'a> Endpoint for Tree<'a> {
    fn method(&self) -> Method {
        Method::GET
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("projects/{}/repository/tree", self.project).into()
    }

    fn parameters(&self) -> QueryParams {
        let mut params = QueryParams::default();

        params.push_opt("path", self.path.as_ref());
        params.push_opt("ref", self.r#ref.as_ref());
        params.push_opt("recursive", self.recursive);

        params
    }
}

impl<'a> Pageable for Tree<'a> {}

#[cfg(test)]
mod tests {
    use crate::api::projects::repository::tree::Tree;
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};
    use http::Method;

    #[test]
    fn project_and_commit_are_necessary() {
        let err = Tree::builder().build().unwrap_err();
        assert_eq!(err, "`project` must be initialized");
    }

    #[test]
    fn project_is_necessary() {
        let err = Tree::builder().build().unwrap_err();
        assert_eq!(err, "`project` must be initialized");
    }

    #[test]
    fn project_is_sufficient() {
        Tree::builder().project(1).build().unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::GET)
            .endpoint("projects/simple%2Fproject/repository/tree")
            .add_query_params(&[("path", "path/to/folder")])
            .add_query_params(&[("ref", "branch")])
            .add_query_params(&[("recursive", "true")])
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = Tree::builder()
            .project("simple/project")
            .path("path/to/folder")
            .r#ref("branch")
            .recursive(true)
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
