// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::str;

use derive_builder::Builder;
use log::warn;

use crate::api::common::{self, NameOrId};
use crate::api::endpoint_prelude::*;
use crate::api::projects::repository::files::Encoding;
use crate::api::ParamValue;

impl Encoding {
    pub fn decode<'a>(self, content: &'a str) -> Cow<'a, str> {
        match self {
            Encoding::Text => content.into(),
            Encoding::Base64 => String::from_utf8(base64::decode(content).unwrap())
                .unwrap()
                .into(),
        }
    }
}

/// Get a file in a project.
#[derive(Debug, Builder)]
#[builder(setter(strip_option))]
pub struct File<'a> {
    /// The project to get the file from.
    #[builder(setter(into))]
    project: NameOrId<'a>,
    /// The path to the file in the repository.
    ///
    /// This is automatically escaped as needed.
    #[builder(setter(into))]
    file_path: Cow<'a, str>,

    /// The reference of the commit.
    #[builder(setter(into))]
    r#ref: Cow<'a, str>,
}

impl<'a> File<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> FileBuilder<'a> {
        FileBuilder::default()
    }
}

const SAFE_ENCODING: Encoding = Encoding::Base64;

impl<'a> Endpoint for File<'a> {
    fn method(&self) -> Method {
        Method::GET
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!(
            "projects/{}/repository/files/{}",
            self.project,
            common::path_escaped(&self.file_path),
        )
        .into()
    }

    fn parameters(&self) -> QueryParams {
        let mut params = QueryParams::default();

        params.push("ref", &self.r#ref);

        params
    }
}

#[cfg(test)]
mod tests {
    use http::Method;

    use crate::api::projects::repository::files::{Encoding, File};
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn all_parameters_are_needed() {
        let err = File::builder().build().unwrap_err();
        assert_eq!(err, "`project` must be initialized");
    }

    #[test]
    fn project_is_required() {
        let err = File::builder()
            .file_path("new/file")
            .r#ref("master")
            .build()
            .unwrap_err();
        assert_eq!(err, "`project` must be initialized");
    }

    #[test]
    fn file_path_is_required() {
        let err = File::builder()
            .project(1)
            .r#ref("master")
            .build()
            .unwrap_err();
        assert_eq!(err, "`file_path` must be initialized");
    }

    #[test]
    fn ref_is_required() {
        let err = File::builder()
            .project(1)
            .file_path("new/file")
            .build()
            .unwrap_err();
        assert_eq!(err, "`r#ref` must be initialized");
    }

    #[test]
    fn sufficient_parameters() {
        File::builder()
            .project(1)
            .file_path("new/file")
            .r#ref("master")
            .build()
            .unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::GET)
            .endpoint("projects/simple%2Fproject/repository/files/path%2Fto%2Ffile")
            .add_query_params(&[("ref", "branch")])
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = File::builder()
            .project("simple/project")
            .file_path("path/to/file")
            .r#ref("branch")
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
