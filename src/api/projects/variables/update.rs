// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;
use crate::api::projects::pipelines::PipelineVariableType;

/// Update a new variable on a project.
#[derive(Debug, Builder)]
pub struct UpdateVariable<'a> {
    /// The project to create the pipeline within.
    #[builder(setter(into))]
    project: NameOrId<'a>,

    /// The name of the variable.
    #[builder(setter(into))]
    pub key: Cow<'a, str>,
    /// The value of the variable.
    #[builder(setter(into))]
    pub value: Cow<'a, str>,
    /// The way the variable should be exposed to pipeline jobs.
    #[builder(default)]
    pub variable_type: PipelineVariableType,
    /// Whether the variable is protected
    #[builder(default)]
    pub protected: Option<bool>,
    /// Whether the variable is masked
    #[builder(default)]
    pub masked: Option<bool>,
}

impl<'a> UpdateVariable<'a> {
    /// Update a builder for the endpoint.
    pub fn builder() -> UpdateVariableBuilder<'a> {
        UpdateVariableBuilder::default()
    }
}

impl<'a> Endpoint for UpdateVariable<'a> {
    fn method(&self) -> Method {
        Method::PUT
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("projects/{}/variables/{}", self.project, self.key).into()
    }

    fn body(&self) -> Result<Option<(&'static str, Vec<u8>)>, BodyError> {
        let mut params = FormParams::default();

        params
            .push("value", &self.value)
            .push("variable_type", self.variable_type.as_str())
            .push_opt("protected", self.protected)
            .push_opt("masked", self.masked);
        params.into_body()
    }
}

#[cfg(test)]
mod tests {
    use http::Method;

    use crate::api::projects::variables::UpdateVariable;
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn project_key_and_value_are_necessary() {
        let err = UpdateVariable::builder().build().unwrap_err();
        assert_eq!(err, "`project` must be initialized");
    }

    #[test]
    fn project_is_necessary() {
        let err = UpdateVariable::builder()
            .key("key")
            .value("value")
            .build()
            .unwrap_err();
        assert_eq!(err, "`project` must be initialized");
    }

    #[test]
    fn key_is_necessary() {
        let err = UpdateVariable::builder()
            .value("value")
            .project(1)
            .build()
            .unwrap_err();
        assert_eq!(err, "`key` must be initialized");
    }

    #[test]
    fn value_is_necessary() {
        let err = UpdateVariable::builder()
            .project(1)
            .key("key")
            .build()
            .unwrap_err();
        assert_eq!(err, "`value` must be initialized");
    }

    #[test]
    fn project_key_and_value_are_sufficient() {
        UpdateVariable::builder()
            .key("key")
            .value("value")
            .project(1)
            .build()
            .unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::PUT)
            .endpoint("projects/simple%2Fproject/variables/key")
            .content_type("application/x-www-form-urlencoded")
            .body_str(concat!("value=value", "&variable_type=env_var"))
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = UpdateVariable::builder()
            .project("simple/project")
            .key("key")
            .value("value")
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
