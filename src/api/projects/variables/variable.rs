// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Query a single variable on a project.
#[derive(Debug, Builder)]
pub struct Variable<'a> {
    /// The project to query for variable.
    #[builder(setter(into))]
    project: NameOrId<'a>,
    /// The key of the variable.
    #[builder(setter(into))]
    key: Cow<'a, str>,
}

impl<'a> Variable<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> VariableBuilder<'a> {
        VariableBuilder::default()
    }
}

impl<'a> Endpoint for Variable<'a> {
    fn method(&self) -> Method {
        Method::GET
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("projects/{}/variables/{}", self.project, self.key).into()
    }
}

#[cfg(test)]
mod tests {
    use crate::api::projects::variables::Variable;
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn project_and_key_are_needed() {
        let err = Variable::builder().build().unwrap_err();
        assert_eq!(err, "`project` must be initialized");
    }

    #[test]
    fn project_is_needed() {
        let err = Variable::builder().key("key").build().unwrap_err();
        assert_eq!(err, "`project` must be initialized");
    }

    #[test]
    fn key_is_needed() {
        let err = Variable::builder().project(1).build().unwrap_err();
        assert_eq!(err, "`key` must be initialized");
    }

    #[test]
    fn project_and_key_are_sufficient() {
        Variable::builder().project(1).key("key").build().unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("projects/simple%2Fproject/variables/key")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = Variable::builder()
            .project("simple/project")
            .key("key")
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
