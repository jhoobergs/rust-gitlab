// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Query for a specific project on an instance.
#[derive(Debug, Builder)]
#[builder(setter(strip_option))]
pub struct ArchiveProject<'a> {
    /// The project to get.
    #[builder(setter(into))]
    project: NameOrId<'a>,

    _un_archive: bool, // false for archive, true for unarchive
}

impl<'a> ArchiveProject<'a> {
    /// Create a builder for the archive endpoint.
    pub fn builder_archive() -> ArchiveProjectBuilder<'a> {
        let mut builder = ArchiveProjectBuilder::default();
        builder._un_archive(false);
        builder
    }

    /// Create a builder for the unarchive endpoint.
    pub fn builder_unarchive() -> ArchiveProjectBuilder<'a> {
        let mut builder = ArchiveProjectBuilder::default();
        builder._un_archive(true);
        builder
    }
}

impl<'a> Endpoint for ArchiveProject<'a> {
    fn method(&self) -> Method {
        Method::POST
    }

    fn endpoint(&self) -> Cow<'static, str> {
        let archive_part = if self._un_archive {
            "unarchive"
        } else {
            "archive"
        };
        format!("projects/{}/{}", self.project, archive_part).into()
    }

    fn parameters(&self) -> QueryParams {
        QueryParams::default()
    }
}

#[cfg(test)]
mod tests {
    mod archive {
        use crate::api::projects::ArchiveProject;
        use crate::api::{self, Query};
        use crate::test::client::{ExpectedUrl, SingleTestClient};
        use http::Method;
        #[test]
        fn project_is_necessary() {
            let err = ArchiveProject::builder_archive().build().unwrap_err();
            assert_eq!(err, "`project` must be initialized");
        }

        #[test]
        fn project_is_sufficient() {
            ArchiveProject::builder_archive()
                .project(1)
                .build()
                .unwrap();
        }

        #[test]
        fn endpoint() {
            let endpoint = ExpectedUrl::builder()
                .method(Method::POST)
                .endpoint("projects/simple%2Fproject/archive")
                .build()
                .unwrap();
            let client = SingleTestClient::new_raw(endpoint, "");

            let endpoint = ArchiveProject::builder_archive()
                .project("simple/project")
                .build()
                .unwrap();
            api::ignore(endpoint).query(&client).unwrap();
        }
    }

    mod unarchive {
        use crate::api::projects::ArchiveProject;
        use crate::api::{self, Query};
        use crate::test::client::{ExpectedUrl, SingleTestClient};
        use http::Method;
        #[test]
        fn project_is_necessary() {
            let err = ArchiveProject::builder_unarchive().build().unwrap_err();
            assert_eq!(err, "`project` must be initialized");
        }

        #[test]
        fn project_is_sufficient() {
            ArchiveProject::builder_unarchive()
                .project(1)
                .build()
                .unwrap();
        }

        #[test]
        fn endpoint() {
            let endpoint = ExpectedUrl::builder()
                .method(Method::POST)
                .endpoint("projects/simple%2Fproject/unarchive")
                .build()
                .unwrap();
            let client = SingleTestClient::new_raw(endpoint, "");

            let endpoint = ArchiveProject::builder_unarchive()
                .project("simple/project")
                .build()
                .unwrap();
            api::ignore(endpoint).query(&client).unwrap();
        }
    }
}
