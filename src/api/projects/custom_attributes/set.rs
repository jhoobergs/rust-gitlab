// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;

/// Set a custom attribute on a project.
#[derive(Debug, Builder)]
#[builder(setter(strip_option))]
pub struct SetProjectCustomAttribute<'a> {
    /// The project to set the custom attribute on.
    #[builder(setter(into))]
    project: NameOrId<'a>,
    /// The key of the custom attribute.
    #[builder(setter(into))]
    key: Cow<'a, str>,
    /// The value of the custom attribute.
    #[builder(setter(into))]
    value: Cow<'a, str>,
}

impl<'a> SetProjectCustomAttribute<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> SetProjectCustomAttributeBuilder<'a> {
        SetProjectCustomAttributeBuilder::default()
    }
}

impl<'a> Endpoint for SetProjectCustomAttribute<'a> {
    fn method(&self) -> Method {
        Method::PUT
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("projects/{}/custom_attributes/{}", self.project, self.key).into()
    }

    fn body(&self) -> Result<Option<(&'static str, Vec<u8>)>, BodyError> {
        let mut params = FormParams::default();

        params.push("value", &self.value);

        params.into_body()
    }
}

#[cfg(test)]
mod tests {
    use http::Method;

    use crate::api::projects::custom_attributes::SetProjectCustomAttribute;
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn project_is_necessary() {
        let err = SetProjectCustomAttribute::builder()
            .key("key")
            .value("value")
            .build()
            .unwrap_err();
        assert_eq!(err, "`project` must be initialized");
    }

    #[test]
    fn key_is_necessary() {
        let err = SetProjectCustomAttribute::builder()
            .project("project")
            .value("value")
            .build()
            .unwrap_err();
        assert_eq!(err, "`key` must be initialized");
    }

    #[test]
    fn value_is_necessary() {
        let err = SetProjectCustomAttribute::builder()
            .project("project")
            .key("key")
            .build()
            .unwrap_err();
        assert_eq!(err, "`value` must be initialized");
    }

    #[test]
    fn project_and_key_and_value_are_sufficient() {
        SetProjectCustomAttribute::builder()
            .project("project")
            .key("key")
            .value("value")
            .build()
            .unwrap();
    }

    #[test]
    fn endpoint() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::PUT)
            .endpoint("projects/simple%2Fproject/custom_attributes/some_key")
            .content_type("application/x-www-form-urlencoded")
            .body_str("value=some_value")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = SetProjectCustomAttribute::builder()
            .project("simple/project")
            .key("some_key")
            .value("some_value")
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }
}
