// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Project variable API endpoints.
//!
//! These endpoints are used for querying CI variables.

mod create;
mod update;
mod variable;

pub use self::create::CreateVariable;
pub use self::create::CreateVariableBuilder;

pub use self::update::UpdateVariable;
pub use self::update::UpdateVariableBuilder;

pub use self::variable::Variable;
pub use self::variable::VariableBuilder;
