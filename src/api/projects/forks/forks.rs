// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::collections::BTreeMap;

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::common::{AccessLevel, SortOrder, VisibilityLevel};
use crate::api::endpoint_prelude::*;
use crate::api::projects::ProjectOrderBy;
/// Query for forks of a project.
#[derive(Debug, Builder)]
#[builder(setter(strip_option))]
pub struct Forks<'a> {
    /// The project to query for forks.
    #[builder(setter(into))]
    project: NameOrId<'a>,

    /// Search for projects using a query string.
    ///
    /// The search query will be escaped automatically.
    #[builder(setter(into), default)]
    search: Option<Cow<'a, str>>,

    /// Filter projects by its archived state.
    #[builder(default)]
    archived: Option<bool>,

    /// Filter projects by its visibility.
    #[builder(default)]
    visibility: Option<VisibilityLevel>,
    /// Return only simple fields for search results.
    #[builder(default)]
    simple: Option<bool>,
    /// Filter projects by those owned by the API caller.
    #[builder(default)]
    owned: Option<bool>,
    /// Filter projects by those the API caller is a member of.
    #[builder(default)]
    membership: Option<bool>,
    /// Filter projects by those the API caller has starred.
    #[builder(default)]
    starred: Option<bool>,
    /// Include project statistics in the results.
    #[builder(default)]
    statistics: Option<bool>,

    /// Filter projects by whether issues are enabled.
    #[builder(default)]
    with_issues_enabled: Option<bool>,
    /// Filter projects by whether merge requests are enabled.
    #[builder(default)]
    with_merge_requests_enabled: Option<bool>,
    /// Filter projects by those where the API caller has a minimum access level.
    #[builder(default)]
    min_access_level: Option<AccessLevel>,

    /// Search for projects with a given custom attribute set.
    #[builder(setter(name = "_custom_attributes"), default, private)]
    custom_attributes: BTreeMap<Cow<'a, str>, Cow<'a, str>>,
    /// Search for projects with custom attributes.
    #[builder(default)]
    with_custom_attributes: Option<bool>,

    /// Order results by a given key.
    #[builder(default)]
    order_by: Option<ProjectOrderBy>,
    /// The sort order for return results.
    #[builder(default)]
    sort: Option<SortOrder>,
}

impl<'a> Forks<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> ForksBuilder<'a> {
        ForksBuilder::default()
    }
}

impl<'a> ForksBuilder<'a> {
    /// Add a custom attribute search parameter.
    pub fn custom_attribute<K, V>(&mut self, key: K, value: V) -> &mut Self
    where
        K: Into<Cow<'a, str>>,
        V: Into<Cow<'a, str>>,
    {
        self.custom_attributes
            .get_or_insert_with(BTreeMap::new)
            .insert(key.into(), value.into());
        self
    }

    /// Add multiple custom attribute search parameters.
    pub fn custom_attributes<I, K, V>(&mut self, iter: I) -> &mut Self
    where
        I: Iterator<Item = (K, V)>,
        K: Into<Cow<'a, str>>,
        V: Into<Cow<'a, str>>,
    {
        self.custom_attributes
            .get_or_insert_with(BTreeMap::new)
            .extend(iter.map(|(k, v)| (k.into(), v.into())));
        self
    }
}

impl<'a> Endpoint for Forks<'a> {
    fn method(&self) -> Method {
        Method::GET
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("projects/{}/forks", self.project).into()
    }

    fn parameters(&self) -> QueryParams {
        let mut params = QueryParams::default();

        params
            .push_opt("search", self.search.as_ref())
            .push_opt("archived", self.archived)
            .push_opt("visibility", self.visibility)
            .push_opt("simple", self.simple)
            .push_opt("owned", self.owned)
            .push_opt("membership", self.membership)
            .push_opt("starred", self.starred)
            .push_opt("statistics", self.statistics)
            .push_opt("with_issues_enabled", self.with_issues_enabled)
            .push_opt(
                "with_merge_requests_enabled",
                self.with_merge_requests_enabled,
            )
            .push_opt(
                "min_access_level",
                self.min_access_level.map(|level| level.as_u64()),
            )
            .extend(
                self.custom_attributes
                    .iter()
                    .map(|(key, value)| (format!("custom_attributes[{}]", key), value)),
            )
            .push_opt("with_custom_attributes", self.with_custom_attributes)
            .push_opt("order_by", self.order_by)
            .push_opt("sort", self.sort);

        params
    }
}

impl<'a> Pageable for Forks<'a> {
    fn use_keyset_pagination(&self) -> bool {
        self.order_by
            .map_or(false, |order_by| order_by.use_keyset_pagination())
    }
}

#[cfg(test)]
mod tests {
    use crate::api::common::{AccessLevel, SortOrder, VisibilityLevel};
    use crate::api::projects::forks::Forks;
    use crate::api::projects::ProjectOrderBy;
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};

    #[test]
    fn project_is_needed() {
        let err = Forks::builder().build().unwrap_err();
        assert_eq!(err, "`project` must be initialized");
    }

    #[test]
    fn project_is_sufficient() {
        Forks::builder().project(1).build().unwrap();
    }

    #[test]
    fn endpoint_project() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("projects/simple%2Fproject/forks")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = Forks::builder().project("simple/project").build().unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }

    macro_rules! test_endpoint {
        ($name:ident, $params:expr, $func:ident, $val:expr) => {
            #[test]
            fn $name() {
                let endpoint = ExpectedUrl::builder()
                    .endpoint("projects/simple%2Fproject/forks")
                    .add_query_params($params)
                    .build()
                    .unwrap();
                let client = SingleTestClient::new_raw(endpoint, "");

                let endpoint = Forks::builder()
                    .project("simple/project")
                    .$func($val)
                    .build()
                    .unwrap();
                api::ignore(endpoint).query(&client).unwrap();
            }
        };
    }

    macro_rules! test_endpoint_s {
        ($name:ident, $func:ident, $val:expr) => {
            test_endpoint!($name, &[(stringify!($func), stringify!($val))], $func, $val);
        };
    }
    macro_rules! test_endpoint_s_string {
        // same but without the stringified $val
        ($name:ident, $func:ident, $val:expr) => {
            test_endpoint!($name, &[(stringify!($func), $val)], $func, $val);
        };
    }

    test_endpoint_s_string!(endpoint_search, search, "special/query");
    test_endpoint_s!(endpoint_archived, archived, false);

    test_endpoint!(
        endpoint_visibility,
        &[("visibility", "private")],
        visibility,
        VisibilityLevel::Private
    );

    test_endpoint_s!(endpoint_simple, simple, false);
    test_endpoint_s!(endpoint_owned, owned, false);
    test_endpoint_s!(endpoint_membership, membership, false);
    test_endpoint_s!(endpoint_starred, starred, false);
    test_endpoint_s!(endpoint_statistics, statistics, false);
    test_endpoint_s!(endpoint_with_issues_enabled, with_issues_enabled, false);
    test_endpoint_s!(
        endpoint_with_merge_requests_enabled,
        with_merge_requests_enabled,
        false
    );
    test_endpoint!(
        endpoint_min_access_level,
        &[("min_access_level", "10")],
        min_access_level,
        AccessLevel::Guest
    );

    #[test]
    fn endpoint_custom_attributes() {
        let endpoint = ExpectedUrl::builder()
            .endpoint("projects/simple%2Fproject/forks")
            .add_query_params(&[
                ("custom_attributes[key]", "value"),
                ("custom_attributes[key2]", "value"),
                ("custom_attributes[key3]", "value&value"),
            ])
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = Forks::builder()
            .project("simple/project")
            .custom_attribute("key", "value")
            .custom_attributes([("key2", "value"), ("key3", "value&value")].iter().cloned())
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }

    test_endpoint_s!(
        endpoint_with_custom_attributes,
        with_custom_attributes,
        true
    );
    test_endpoint!(
        endpoint_order_by,
        &[("order_by", "id")],
        order_by,
        ProjectOrderBy::Id
    );
    test_endpoint!(
        endpoint_sort,
        &[("sort", "desc")],
        sort,
        SortOrder::Descending
    );
}
