// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;

use crate::api::common::NameOrId;
use crate::api::endpoint_prelude::*;
/// Query for forks of a project.
#[derive(Debug, Builder)]
#[builder(setter(strip_option))]
pub struct CreateFork<'a> {
    /// The project to fork.
    #[builder(setter(into))]
    from: NameOrId<'a>,

    /// The ID of the namespace that the project will be forked to
    #[builder(default)]
    namespace_id: Option<u64>,

    /// The path of the namespace that the project will be forked to
    #[builder(setter(into), default)]
    namespace_path: Option<Cow<'a, str>>,

    /// The path that will be assigned to the resultant project after forking
    #[builder(setter(into), default)]
    path: Option<Cow<'a, str>>,

    /// The name that will be assigned to the resultant project after forking
    #[builder(setter(into), default)]
    name: Option<Cow<'a, str>>,
}

impl<'a> CreateFork<'a> {
    /// Create a builder for the endpoint.
    pub fn builder() -> CreateForkBuilder<'a> {
        CreateForkBuilder::default()
    }
}

impl<'a> Endpoint for CreateFork<'a> {
    fn method(&self) -> Method {
        Method::POST
    }

    fn endpoint(&self) -> Cow<'static, str> {
        format!("projects/{}/fork", self.from).into()
    }

    fn parameters(&self) -> QueryParams {
        let mut params = QueryParams::default();

        params
            .push_opt("namespace_id", self.namespace_id)
            .push_opt("namespace_path", self.namespace_path.as_ref())
            .push_opt("path", self.path.as_ref())
            .push_opt("name", self.name.as_ref());

        params
    }
}

#[cfg(test)]
mod tests {
    use crate::api::common::{AccessLevel, SortOrder, VisibilityLevel};
    use crate::api::projects::forks::CreateFork;
    use crate::api::projects::ProjectOrderBy;
    use crate::api::{self, Query};
    use crate::test::client::{ExpectedUrl, SingleTestClient};
    use http::Method;

    #[test]
    fn from_is_needed() {
        let err = CreateFork::builder().build().unwrap_err();
        assert_eq!(err, "`from` must be initialized");
    }

    #[test]
    fn from_is_sufficient() {
        CreateFork::builder().from(1).build().unwrap();
    }

    #[test]
    fn endpoint_from() {
        let endpoint = ExpectedUrl::builder()
            .method(Method::POST)
            .endpoint("projects/simple%2Fproject/fork")
            .build()
            .unwrap();
        let client = SingleTestClient::new_raw(endpoint, "");

        let endpoint = CreateFork::builder()
            .from("simple/project")
            .build()
            .unwrap();
        api::ignore(endpoint).query(&client).unwrap();
    }

    macro_rules! test_endpoint {
        ($name:ident, $params:expr, $func:ident, $val:expr) => {
            #[test]
            fn $name() {
                let endpoint = ExpectedUrl::builder()
                    .method(Method::POST)
                    .endpoint("projects/simple%2Fproject/fork")
                    .add_query_params($params)
                    .build()
                    .unwrap();
                let client = SingleTestClient::new_raw(endpoint, "");

                let endpoint = CreateFork::builder()
                    .from("simple/project")
                    .$func($val)
                    .build()
                    .unwrap();
                api::ignore(endpoint).query(&client).unwrap();
            }
        };
    }

    macro_rules! test_endpoint_s {
        ($name:ident, $func:ident, $val:expr) => {
            test_endpoint!($name, &[(stringify!($func), stringify!($val))], $func, $val);
        };
    }
    macro_rules! test_endpoint_s_string {
        // same but without the stringified $val
        ($name:ident, $func:ident, $val:expr) => {
            test_endpoint!($name, &[(stringify!($func), $val)], $func, $val);
        };
    }

    test_endpoint_s!(endpoint_namespace_id, namespace_id, 10);
    test_endpoint_s_string!(endpoint_namespace_path, namespace_path, "special/query");
    test_endpoint_s_string!(endpoint_path, path, "special/query");
    test_endpoint_s_string!(endpoint_name, name, "special/query");
}
